package eelects.collection.entities;

import javax.persistence.*;
import java.util.Date;

@Entity
public class Vote {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long id;
    @ManyToOne
    @JoinColumn(name = "studentId")
    private Student student;
    @ManyToOne
    @JoinColumn(name = "candidatId")
    private Candidat candidat;

    @Column
    private int NbrVote;
    @Column
    private Date dateVote;

    /*
    public Vote(Long id, Candidat candidat, Student student, int nbrVote, Date dateVote) {
        this.id = id;
        this.candidat = candidat;
        this.student = student;
        this.NbrVote = nbrVote;
        this.dateVote = dateVote;
    }
*/
  //  public Vote() {}


    public Student getStudent() {
        return student;
    }

    public void setStudent(Student student) {
        this.student = student;
    }

    public Candidat getCandidat() {
        return candidat;
    }

    public void setCandidat(Candidat candidat) {
        this.candidat = candidat;
    }

    public int getNbrVote() {
        return NbrVote;
    }

    public void setNbrVote(int nbrVote) {
        NbrVote = nbrVote;
    }

    public Date getDateVote() {
        return dateVote;
    }

    public void setDateVote(Date dateVote) {
        this.dateVote = dateVote;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
