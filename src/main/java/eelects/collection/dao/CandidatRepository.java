package eelects.collection.dao;


import eelects.collection.entities.Candidat;

import org.springframework.data.jpa.repository.JpaRepository;

public interface CandidatRepository extends JpaRepository<Candidat, Long> {
}
