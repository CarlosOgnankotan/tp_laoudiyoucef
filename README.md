# tp-E-Elect

E-Elect est un WebAPi qui permet de choisir et de voter un délégué  ou un responsable pour un groupe d'étudiants.
# Modelisation du projet
Nous avons (03) trois classes dans notre projet qui sont :
- Student ;
- Candidat ;
- Vote.

![Previous Architecuture](/assets/mod.png)

# Architecture du projet
Notre projet fonctionne en 3tiers. L'architecture trois tiers1, aussi appelée architecture à trois niveaux ou architecture à trois couches, est l'application du modèle plus général qu'est le multi-tiers. L'architecture logique du système est divisée en trois niveaux ou couches :
- couche de présentation ;
- couche de traitement ;
- couche d'accès aux données.

C'est une architecture basée sur l'environnement client–serveur.

![Previous Architecuture](/assets/arch.png)

# Quelques commandes à exécuter pour compiler le projet
```bash 
 mvn compile
```
**_mvn compile_**   compile le code source du projet.

```bash 
 mvn test-compile
```
**_mvn test-compile_**  compile le code source test du projet.

```bash 
 mvn test
```
**_mvn test_**  exécute des tests pour le projet.

```bash 
 mvn package
```
**_mvn package_** crée un fichier JAR ou WAR pour le projet afin de le convertir en un format distribuable.
```bash 
 mvn install
```
**_mvn install_**  déploie le fichier JAR/WAR packagé dans le référentiel local.



