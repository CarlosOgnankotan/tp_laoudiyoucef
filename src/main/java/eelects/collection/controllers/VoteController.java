package eelects.collection.controllers;

import eelects.collection.entities.Candidat;
import eelects.collection.entities.Vote;
import eelects.collection.service.VoteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/v1/vote")
public class VoteController {
    @Autowired
    private VoteService voteService;

    @PostMapping
    public boolean postVote(@PathVariable Long studentId, @PathVariable Long candidaId){
        try {
            if (studentId != null && candidaId != null) {
                return voteService.addVote(studentId,candidaId);
            } else
                return false;
        }
        catch (Exception e){
            System.out.println(e.getMessage());
            return false;
        }
    }
}
